import { create } from 'zustand'
import { devtools, persist, createJSONStorage} from 'zustand/middleware'

interface CounterState {
  add: number
  delete: number
  sum: number
  actionAdd: () => void
  actionDelete: () => void
  actionReset: () => void
  actionSum: ()=> void
}


const initState = {
  add: 0,
  delete: 0,
  sum: 0,
  reset: 0,
}
const useClickStore = create<CounterState>()(
  devtools(
    persist(
      (set, get) => ({
        add: 0,
        delete: 0,
        sum: 0,
        reset: 0,
        actionAdd: () => set((state) => {
          get().actionSum()
          return {
            add: state.add + 1,
          }
        }),
        actionDelete: () => set((state) => {
          get().actionSum()
          return {
            delete: state.delete + 1,
          }
        }),
        actionReset: () => set(() => {
          get().actionSum()
          return {
            ...initState
          }
        }),
        actionSum: () => set((state) => {
          return {
            sum: state.add + state.delete
          }
        }),
      }),
      {
        name: 'counter-storage',
      }
    )
  )
)

export default useClickStore