import useClickStore from './click'
import useCounterStore from './counter'

export  {
  useClickStore,
  useCounterStore
}