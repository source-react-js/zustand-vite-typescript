import { create } from 'zustand'
import { devtools } from 'zustand/middleware'

interface CounterState {
  number: number
  increase: (number: number) => void
  decrease: (number: number) => void
  reset: () => void
}

const useCounterStore = create<CounterState>()(
  devtools(
      (set) => ({
        number: 0,
        increase: (value) => set((state) => {
          return {
            number: state.number + value,
          }
        }),
        decrease: (value) => set((state) => {
          return {
            number: state.number - value,
          }
        }),
        reset: () =>set(() => {
          return {
            number: 0
          }
        }),
      }),
      {
        name: 'counter-storage',
      }
  )
)

export default useCounterStore