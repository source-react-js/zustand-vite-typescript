import './App.css'
import {
  useCounterStore,
  useClickStore
} from './store'
function App() {
  const counter = useCounterStore()
  const click = useClickStore()
  return (
    <div>
      Counter = {counter.number}
      <div className='btn'>
        <button onClick={() => {
          counter.increase(1)
          click.actionAdd()
        }}>
          +
        </button>
        <button onClick={() => {
          counter.decrease(1)
          click.actionDelete()
        }}>
          -
        </button>
        <button onClick={() => {
          counter.reset()
          click.actionReset()
        }}>
          Reset
        </button>
      </div>
      <div>
        Sum of click => {click.add + click.delete}
      </div>
      <div>
        [ Add : {click.add} ] , [ Delete : {click.delete} ]
      </div>
    </div>
  )
}

export default App
